#! /usr/bin/env python3
# NACA.py
"""
The purpose of this program is to generate NACA 4-Digit airfoils using
"""
import airfoil_lib


def main():
    number = '63-215'
    chord = 2
    naca = airfoil_lib.NACA6(number, chord, a=0.7)
    print(repr(naca))
    print(naca)
    naca.preview()


if __name__ == "__main__":
    main()
