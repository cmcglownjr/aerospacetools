# NACA4.py

from airfoil_lib import Airfoil
import numpy as np


class NACA4(Airfoil):
    """Contains the properties and methods for the NACA 4-Digit series of airfoils."""

    def __init__(self, number, chord, iteration=1000):
        super().__init__(number, chord, iteration)
        self.M = float(self.number[0]) / 100
        self.P = float(self.number[1]) / 10
        self.T = float(self.number[2:4]) / 100

    def __repr__(self):
        """ This method recreates the input for this class"""
        return "airfoil_lib.NACA4('{}', {}, {})".format(self.number, self.c, self.iter)

    def __str__(self):
        """This method prints the properties of the NACA 4 series airfoil given."""
        if self.number[0:2] == '00':  # For symmetric Airfoils
            msg = "This airfoil, the NACA {}, is a symmetric airfoil with a maximum thickness of {:.1f}% of the " \
                  "chord. The chord length is {:.1f} meters".format(self.number, self.T * 100, self.c)
        else:
            msg = "This airfoil, the NACA {}, has a maximum camber of {:.1f}% chord located {:.1f}% from the leading " \
                  "edge with a maximum thickness of {:.1f}% of the chord. The chord length is {:.1f} " \
                  "meters.".format(self.number, self.M * 100, self.P * 100, self.T * 100, self.c)
        return msg

    def coordinate(self, x):
        """This method calculates the camber and gradients of the airfoil."""
        if self.number[0:2] == '00':  # For symmetric airfoils
            y_c = 0
            dyc_dx = 0
        else:
            if 0 <= x < self.P * self.c:
                y_c = self.M * (x / np.power(self.P, 2)) * (2 * self.P - x / self.c)
                dyc_dx = ((2 * self.M) / np.power(self.P, 2)) * (self.P - x / self.c)
            else:
                y_c = self.M * ((self.c - x) / np.power(1 - self.P, 2)) * (1 + x / self.c - 2 * self.P)
                dyc_dx = ((2 * self.M) / np.power(1 - self.P, 2)) * (self.P - x / self.c)
        return y_c, dyc_dx
