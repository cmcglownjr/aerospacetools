# Airfoil.py

"""
The purpose of this file is to generate the class structure for various airfoil data. These include the NACA series of
airfoil. This is a work in progress.
"""

import numpy as np
from matplotlib import pyplot as plt


class Airfoil:
    """Original class for airfoil templates"""

    def __init__(self, number, chord=1, iteration=100):
        self.number = str(number)
        self.c = float(chord)
        self.iter = iteration

    def __repr__(self):
        pass

    def __str__(self):
        pass

    def xy_coordinate(self, x):
        """This method computes the upper and lower xy coordinates of the NACA airfoil"""
        tdc = [0.2969, -0.126, -0.3516, 0.2843, -0.1036]  # These are the thickness distribution constants
        y_t = 5 * self.T * self.c * (tdc[0] * np.sqrt(x / self.c) + tdc[1] * (x / self.c) +
                                     tdc[2] * np.power(x / self.c, 2) + tdc[3] * np.power(x / self.c, 3) +
                                     tdc[4] * np.power(x / self.c, 4))
        y_c, dyc_dx = self.coordinate(x)
        theta = np.arctan(dyc_dx)
        xu = x - y_t * np.sin(theta)
        yu = y_c + y_t * np.cos(theta)
        xl = x + y_t * np.sin(theta)
        yl = y_c - y_t * np.cos(theta)
        return xu, yu, xl, yl, y_c

    def preview(self):
        """This method creates a preview of the airfoil normalized by the chord length."""
        eps = np.finfo(np.float64).eps
        x = np.linspace(eps, self.c, self.iter)
        xu = np.linspace(eps, self.c, self.iter)
        yu = np.linspace(eps, self.c, self.iter)
        xl = np.linspace(eps, self.c, self.iter)
        yl = np.linspace(eps, self.c, self.iter)
        yc = np.linspace(eps, self.c, self.iter)
        for i in range(len(x)):
            xu[i], yu[i], xl[i], yl[i], yc[i] = self.xy_coordinate(x[i])
        plt.plot(xu / self.c, yu / self.c, xl / self.c, yl / self.c, x / self.c, yc / self.c, 'r--')
        plt.ylabel('y/c')
        plt.xlabel('x/c')
        plt.title('NACA {}'.format(self.number))
        plt.grid(True, which='both')
        plt.axhline()
        plt.axvline()
        plt.axis('scaled')
        plt.show()
