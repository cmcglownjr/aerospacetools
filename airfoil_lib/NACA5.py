# NACA5.py

from airfoil_lib import Airfoil
import numpy as np


class NACA5(Airfoil):
    """Contains the properties and methods for the NACA 5-Digit series of airfoils, both Non-Reflexed and Reflexed"""

    def __init__(self, number, chord, iteration=1000):
        super().__init__(number, chord, iteration)
        self.L = float(self.number[0])
        self.P = float(self.number[1])
        self.Q = float(self.number[2])
        self.T = float(self.number[3:5]) / 100

    def __repr__(self):
        """ This method recreates the input for this class"""
        return "airfoil_lib.NACA5('{}', {}, {})".format(self.number, self.c, self.iter)

    def __str__(self):
        """This method prints the properties of the NACA 5 series airfoil given."""
        if self.Q == 0:  # Non-Reflexed
            msg = "This airfoil, the NACA {}, is an airfoil with a design lift coefficient of {:.1f}, the position of" \
                  " maximum camber is located at {:.2f}% the chord length,\nand a maximum thickness of {:.1f}% of " \
                  "chord length. This airfoil has a non-reflexed chamber with a chord length of {:.1f} meters." \
                .format(self.number, self.L * 0.15, self.P / 20, self.T * 100, self.c)
        else:
            msg = "This airfoil, the NACA {}, is an airfoil with a design lift coefficient of {:.1f}, the position of" \
                  " maximum camber is located at {:.2f}% the chord length,\nand a maximum thickness of {:.1f}% of " \
                  "chord length. This airfoil has a reflexed chamber with a chord length of {:.1f} meters." \
                .format(self.number, self.L * 0.15, self.P / 20, self.T * 100, self.c)
        return msg

    def coordinate(self, x):
        """This method calculates the camber and gradients of the airfoil."""
        naca5dict = {'210': [0.05, 0.0580, 361.40],  # Chart for Non-Reflexed
                     '220': [0.10, 0.126, 51.640],  # [p, r, k1]
                     '230': [0.15, 0.2025, 15.957],
                     '240': [0.20, 0.290, 6.643],
                     '250': [0.25, 0.391, 3.230],
                     '221': [0.10, 0.130, 51.990, 0.000764],  # Chart for Reflexed
                     '231': [0.15, 0.217, 15.793, 0.00677],  # [p, r, k1, k2/k1]
                     '241': [0.20, 0.318, 6.520, 0.0303],
                     '251': [0.25, 0.441, 3.191, 0.1355]}
        z = naca5dict[self.number[0:3]]
        if self.Q == 0:  # Non-Reflexed
            if 0 <= x / self.c < z[1]:
                y_c = (z[2] / 6) * (np.power(x / self.c, 3) - 3 * z[1] * np.power(x / self.c, 2)
                                    + np.power(z[1], 2) * (3 - z[1]) * x / self.c)
                dyc_dx = (z[2] / 6) * (3 * np.power(x / self.c, 2) - 6 * z[1] * x / self.c + np.power(z[1], 2)
                                       * (3 - z[1]))
            else:
                y_c = (z[2] * np.power(z[1], 3) / 6) * (1 - x / self.c)
                dyc_dx = -(z[2] * np.power(z[1], 3)) / 6
        else:  # Reflexed
            if 0 <= x / self.c < z[1]:
                y_c = (z[2] / 6) * (np.power(x / self.c - z[1], 3) - z[3] * np.power((1 - z[1]), 3) * x / self.c -
                                    np.power(z[1], 3) * x / self.c + np.power(z[1], 3))
                dyc_dx = (z[2] / 6) * (3 * np.power(x / self.c - z[1], 2) - z[3] * np.power(1 - z[1], 3)
                                       - np.power(z[1], 3))
            else:
                y_c = (z[2] / 6) * (z[3] * np.power(x / self.c - z[1], 3) - z[3] * np.power(1 - z[1], 3) * x / self.c -
                                    np.power(z[1], 3) * x / self.c + np.power(z[1], 3))
                dyc_dx = (z[2] / 6) * (3 * z[3] * np.power(x / self.c - z[1], 2) - z[3] * np.power(1 - z[1], 3)
                                       - np.power(z[1], 3))
        return y_c, dyc_dx
